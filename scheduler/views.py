from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.views.generic.base import TemplateView

from .calendar import Calendar, get_date


class DashboardView(TemplateView):
    template_name = "dashboard.html"

    def get(self, request):
        today = get_date()

        prev_month = get_date(month=today.month - 1)
        next_month = get_date(month=today.month + 1)

        cal = Calendar(today)
        html_cal = cal.formatmonth()
        result_cal = mark_safe(html_cal)  # Autoescape off

        context = {
            "calendar": result_cal,
            "prev_month": prev_month,
            "next_month": next_month,
        }

        return render(request, "dashboard.html", context)
