from calendar import HTMLCalendar
import datetime

from .models import Event


def get_date(year=None, month=None, day=1):
    """return type <class 'datetime.datetime' 2020-07-24 01:48:15.227572"""

    year = datetime.datetime.today().year if year is None else year
    month = datetime.datetime.today().month if month is None else month

    return datetime.date(year, month, day)


class Calendar(HTMLCalendar):
    def __init__(self, date):
        self.year = date.year
        self.month = date.month
        super().__init__()

    def formatday(self, day, events):
        events_per_day = events.filter(start_time__day=day)
        d = ""
        for event in events_per_day:
            d += f"<li> {event.get_html_url} </li>"

        if day != 0:
            return f"<td><span class='date'>{day}</span><ul class='event_line'> {d} </ul></td>"
        else:
            return "<td></td>"

    def formatweek(self, theweek, events):
        week = ""
        for day, weekday in theweek:
            week += self.formatday(day, events)

        return f"<tr> {week} </tr>"

    def formatmonth(self, withyear=True):
        events = Event.objects.filter(
            start_time__year=self.year, start_time__month=self.month,
        )

        cal = f"<table class='calendar'>\n"
        cal += f"{self.formatmonthname(self.year, self.month, withyear)}\n"
        cal += f"{self.formatweekheader()}\n"
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f"{self.formatweek(week, events)}\n"
        cal += "</table>"

        return cal
